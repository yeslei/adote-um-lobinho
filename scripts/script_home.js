const fetchConfig = {
  method: 'GET'
}

// funçao de numero aleatorio
function randomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}


fetch("http://localhost:3000/lobinhos", fetchConfig)
  .then(resposta =>

    resposta.json()

      .then(lobinhos => {

        var lobo_escolhido = randomInt(0, lobinhos.length)
        constructor(
          lobinhos[lobo_escolhido].imagem,
          lobinhos[lobo_escolhido].nome,
          lobinhos[lobo_escolhido].idade,
          lobinhos[lobo_escolhido].descricao
        )

        var lobo_escolhido_reverse = randomInt(0, lobinhos.length)
        constructorReversed(
          lobinhos[lobo_escolhido_reverse].imagem,
          lobinhos[lobo_escolhido_reverse].nome,
          lobinhos[lobo_escolhido_reverse].idade,
          lobinhos[lobo_escolhido_reverse].descricao
        )

      })
      .catch(error => console.log(error))
  )
  .catch(erro => console.log(error))

// passar para a página 

function constructor(foto, nome, idade, descricao) {

  let img_lobo = document.querySelector('#principalImg')
  let nome_lobo = document.querySelector('#nomeDoLobo')
  let idade_lobo = document.querySelector('#idadeDoLobo')
  let descricao_lobo = document.querySelector('.paragrafoLobo')

  img_lobo.src = foto
  nome_lobo.innerText = nome
  idade_lobo.innerText = `Idade: ${idade} anos`
  descricao_lobo.innerText = descricao

}

function constructorReversed(foto, nome, idade, descricao) {

  let img_lobo = document.querySelector('#principalImg-reverse')
  let nome_lobo = document.querySelector('#nomeDoLobo-reverse')
  let idade_lobo = document.querySelector('#idadeDoLobo-reverse')
  let descricao_lobo = document.querySelector('.paragrafoLobo-reverse')

  img_lobo.src = foto
  nome_lobo.innerText = nome
  idade_lobo.innerText = `Idade: ${idade} anos`
  descricao_lobo.innerText = descricao

}