const fetchConfig = {
  method: "GET"
};

const pageSize = 5; 
let currentPage = 1; 
const fetchAndRenderData = (pageNumber = 1, searchTerm = '') => {
  fetch(`http://localhost:3000/lobinhos`, fetchConfig)
    .then(response => {
      if (!response.ok) {
        throw new Error('Erro ao obter os dados');
      }
      return response.json();
    })
    .then(data => {
      console.log(data); 
      const filteredData = filterLobos(data, searchTerm);
      renderizarLobos(filteredData, pageNumber);
      renderizarPaginacao(filteredData.length);
    })
    .catch(error => {
      console.error('Erro:', error);
    });
};

const filterLobos = (data, searchTerm) => {
  return data.filter(lobo => lobo.nome.toLowerCase().includes(searchTerm.toLowerCase()));
};

const renderizarLobos = (data, pageNumber) => {
  
  const lobosContainer = document.querySelector('.lobos');
  lobosContainer.innerHTML = '';

  const checkboxAdotados = document.getElementById('adotados');
  const mostrarAdotados = checkboxAdotados.checked;
  
  const lobosFiltrados = data.filter(lobo => mostrarAdotados ? lobo.adotado : !lobo.adotado);

  const startIndex = (pageNumber - 1) * pageSize;
  const endIndex = pageNumber * pageSize;

  let reverse = false; 

  lobosFiltrados.slice(startIndex, endIndex).forEach((lobo, index) => {
    const loboDiv = document.createElement('div');
    loboDiv.classList.add('lobo');
    if (reverse) {
      loboDiv.classList.add('lobo-reverse');
    }
    reverse = !reverse; 

    const imgLobo = document.createElement('div');
    imgLobo.classList.add('imgLobo');
    if (reverse) {
      imgLobo.classList.add('imgLobo-reverse');
    }

    const detalheImagem = document.createElement('img');
    detalheImagem.id = 'detalheImagem';
    detalheImagem.src = 'assets/Detalhe da Imagem.svg';
    detalheImagem.alt = '';

    const principalImg = document.createElement('img');
    principalImg.id = (index % 2 !== 0) ? 'principalImg-reverse' : 'principalImg';
    principalImg.src = lobo.imagem;
    principalImg.alt = lobo.nome;

    imgLobo.appendChild(detalheImagem);
    imgLobo.appendChild(principalImg);

    const cardTodo = document.createElement('div');
    cardTodo.classList.add('cardTodo');
    const buttonAdotar = document.createElement('div');
    buttonAdotar.classList.add('buttonAdotar');
    const informacoes = document.createElement('div');
    informacoes.classList.add('informacoes');
    const nomeDoLobo = document.createElement('h2');
    nomeDoLobo.classList.add('nomeDoLobo');
    nomeDoLobo.textContent = lobo.nome;
    const idadeDoLobo = document.createElement('p');
    idadeDoLobo.classList.add('idadeDoLobo');
    idadeDoLobo.textContent = `Idade: ${lobo.idade} anos`;
    informacoes.appendChild(nomeDoLobo);
    informacoes.appendChild(idadeDoLobo);
    const adotarLink = document.createElement('a');
    adotarLink.classList.add('adotar');
    adotarLink.href = `index_show.html?id=${lobo.id}`; 
    adotarLink.textContent = (lobo.adotado) ? 'Adotado' : 'Adotar';
    if (lobo.adotado) {
      adotarLink.classList.add('adotarAdotado');
    }
    buttonAdotar.appendChild(informacoes);
    buttonAdotar.appendChild(adotarLink);

    const paragrafoLobo = document.createElement('p');
    paragrafoLobo.classList.add('paragrafoLobo');
    paragrafoLobo.textContent = lobo.descricao;

    const adotadoInfo = document.createElement('h3');
    adotadoInfo.classList.add('adotado');
    if (lobo.adotado) {
      adotadoInfo.textContent = `Adotado por: ${lobo.nomeDono}`;
    } else {
      adotadoInfo.textContent = '';
      adotadoInfo.classList.add('naoAdotado');
    }

    cardTodo.appendChild(buttonAdotar);
    cardTodo.appendChild(paragrafoLobo);
    cardTodo.appendChild(adotadoInfo);

    loboDiv.appendChild(imgLobo);
    loboDiv.appendChild(cardTodo);

    lobosContainer.appendChild(loboDiv);
  });
};

const renderizarPaginacao = (totalItems) => {
  
  const totalPages = Math.ceil(totalItems / pageSize);

  const paginationContainer = document.querySelector('.pagination');
  paginationContainer.innerHTML = '';

  const currentPageText = document.createElement('span');
  currentPageText.textContent = `Página ${currentPage}`;
  paginationContainer.appendChild(currentPageText)

  
  const previousButton = document.createElement('button');
  previousButton.classList.add('page-btn');
  previousButton.textContent = 'Anterior';
  previousButton.addEventListener('click', () => {
    if (currentPage > 1) {
      currentPage--;
      fetchAndRenderData(currentPage);
    }
  });
  paginationContainer.appendChild(previousButton);


  const nextButton = document.createElement('button');
  nextButton.classList.add('page-btn');
  nextButton.textContent = 'Próximo';
  nextButton.addEventListener('click', () => {
    if (currentPage < totalPages) {
      currentPage++;
      fetchAndRenderData(currentPage);
    }
  });
  
  paginationContainer.appendChild(nextButton);
  
};

const checkboxAdotados = document.getElementById('adotados');
checkboxAdotados.addEventListener('change', () => {
  fetchAndRenderData(currentPage);
});

const searchBar = document.getElementById('barradepesquisa');
searchBar.addEventListener('input', () => {
  fetchAndRenderData(currentPage, searchBar.value);
})


fetchAndRenderData(currentPage);
