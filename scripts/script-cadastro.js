
let tamanhoGlobal;

function totalLista() {
  let fetchConfig = {
    method: "GET"
  };

  return fetch("http://localhost:3000/lobinhos", fetchConfig)
    .then(response => response.json())
    .then(jsonData => {
      tamanhoGlobal = jsonData.length; 
      return tamanhoGlobal; 
    })
    .catch(error => {
      console.log('Erro:', error);
      throw error; 
    });
}

function postFormulario (){

  salvar = document.querySelector("#salvar")
  salvar.addEventListener('click', mapear => {
    let nomedolobinho = document.querySelector("#nomedolobinho").value
    let anos = document.querySelector("#anos").value
    let foto = document.querySelector("#foto").value
    let descricao = document.querySelector("#descricao").value

    let fetchBody = {
      "id": tamanhoGlobal + 1,
      "nome": nomedolobinho,
      "idade": anos,
      "descricao": descricao,
      "imagem": foto,
      "adotado": false,
      "nomeDono": null,
      "idadeDono": null,
      "emailDono": null
    };
    

    let fetchConfig = {
      method: "POST",
      body: JSON.stringify(fetchBody),
      headers: {"Content-Type" : "application/json"}
    }
    fetch("http://localhost:3000/lobinhos", fetchConfig)
      .then(resposta=> {
        resposta.json()
          .then(resposta => {
            alert(`Você adicionou o lobinho: ${nomedolobinho}`)
          })
          .catch(erro=> {
            console.log(erro)
          })
      })


  }
  )
  


  
}

totalLista()
  .then(total => { postFormulario()
   
    }
  )
  .catch(error => {
  
  });
