document.addEventListener('DOMContentLoaded', function() {
    let idDoLobo;

    // Obter parâmetros de consulta da URL
    const urlParams = new URLSearchParams(window.location.search);

    // Verificar se existe um parâmetro chamado 'id'
    if (urlParams.has('id')) {
        // Obter o valor do parâmetro 'id'
        idDoLobo = urlParams.get('id');
        
        requisicao(); // Chamar a função requisicao() aqui

    } else {
        console.error('Parâmetro "id" não encontrado na URL.');
    }

    function requisicao() {
        // Verificar se idDoLobo está definido
        if (!idDoLobo) {
            console.error('ID do lobo não está definido.');
            return;
        }

        fetch("http://localhost:3000/lobinhos/" + idDoLobo)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Erro ao recuperar dados do servidor');
                }
                return response.json();
            })
            .then(lobo => {
                // Aqui você pode processar os dados do lobo
                console.log('Dados do lobo:', lobo);
            })
            .catch(error => {
                console.error("Erro durante a requisição:", error);
            });
    }

    let formCliente = document.getElementById('formCliente');

    formCliente.addEventListener('submit', function(event) {
        event.preventDefault();

        let nome = document.querySelector('.texto-sobre-linha').value;
        let idade = document.querySelector('.idade-linha').value;
        let email = document.querySelector('.email-sobre-linha').value;

        if (nome === "" || idade === "" || email === "") {
            alert('Preencha todos os campos');
        } else {
            console.log('Nome: ' + nome);
            console.log('Idade: ' + idade);
            console.log('E-mail: ' + email);
            console.log('ID do Lobo: ' + idDoLobo); // Aqui está o ID do lobo

            // Você pode fazer a requisição novamente aqui, se necessário
            // requisicao(); 

            document.querySelector('.texto-sobre-linha').value = '';
            document.querySelector('.idade-linha').value = '';
            document.querySelector('.email-sobre-linha').value = '';
        }
    });
});

//Requisição patch para alterar o status dos lobos adotados

const express = require('express');
const bodyParser = require('body-parser'); //body-parser é configurado para analisar o corpo das requisições HTTP,
const fs = require('fs');

const app = express();
const port = 3000;

app.use(bodyParser.json());

// Rota para manipular requisições PATCH
app.patch('/lobinhos/:id', (req, res) => {
    const id = req.params.id;
    const novoStatus = req.body.status; 

    // Ler o arquivo JSON
    fs.readFile('lobinhos.json', 'utf8', (err, data) => {
        if (err) {
            console.error(err);
            return res.status(500).send('Erro ao ler o arquivo JSON');
        }

        let lobinhos = JSON.parse(data);

        const loboIndex = lobinhos.findIndex(lobo => lobo.id === id);

        if (loboIndex === -1) {
            return res.status(404).send('Lobo não encontrado');
        }

        lobinhos[loboIndex].status = novoStatus;

        // Escrever de volta no arquivo JSON
        fs.writeFile('lobinhos.json', JSON.stringify(lobinhos, null, 2), 'utf8', (err) => {
            if (err) {
                console.error(err);
                return res.status(500).send('Erro ao escrever no arquivo JSON');
            }

            return res.status(200).send('Status do lobo atualizado com sucesso');
        });
    });
});

app.listen(port, () => {
    console.log(`Servidor está rodando em http://localhost:${port}`);
});
